#!/usr/bin/env python

import os
import sys
import uuid

from csv import DictReader

import click
import yaml

import cert_maker


def print_certificates(conf: dict, csv_file, payload: dict):
    # create a folder to place files while being processed temporarily
    build_folder = conf["build_dir"]
    if not os.path.isdir(build_folder):
        os.makedirs(build_folder)

    # create folder to store the pdf output files
    folder_name = "_".join(payload["event"].lower().split(" "))
    output_path = os.path.join(conf["output_dir"], folder_name)
    if not os.path.isdir(output_path):
        os.makedirs(output_path)

    # process each entry from the input csv file
    for position, record in enumerate(DictReader(csv_file), start=1):
        click.secho(f"Processing Certificate No: {position}", fg="green")
        # a random temporary filename for intermediary html file
        int_filename = uuid.uuid4().hex + ".html"
        build_file = os.path.join(build_folder, int_filename)

        # output filename and path
        output_filename = f"{position}-{record['name']}.pdf"
        output_file = os.path.join(output_path, output_filename)

        payload["name"] = record["name"]
        payload["position"] = position
        payload["source"] = build_file
        payload["destination"] = output_file

        print_certificate(pdf_options=conf["pdf"], payload=payload)


def print_certificate(pdf_options: dict, payload: dict):
    # render template
    cert_html = cert_maker.render_template(payload["template"], **payload)
    # store it in build folder
    with open(payload["source"], "w") as fd:
        fd.write(cert_html)
    # generate pdf and save it to output directory
    cert_maker.generate_pdf(
        html_file=payload["source"],
        output_file=payload["destination"],
        options=pdf_options,
    )
    # remove intermediate source file
    os.remove(payload["source"])


@click.command()
@click.option("--conf", default="settings.yaml", help="configuration file")
@click.option("--event", required=True, type=str, help="Name of the event")
@click.option(
    "--event-date", required=True, type=str, help="When did the event happen?"
)
@click.option("--organizer", required=True, type=str, help="Who organized this event?")
@click.option(
    "--signatory",
    required=True,
    default="President, FSFTN",
    help="who is signing this certificate?",
)
@click.option("--venue", required=True, type=str, help="Where did this event happen?")
@click.option("--template", default="certificate.html", help="certificate template")
@click.argument("csv", type=click.File("r"))
def cli(conf, event, event_date, organizer, venue, template, signatory, csv):
    """create bulk certificates from templates and csv file"""
    with open(conf) as fd:
        config = yaml.safe_load(fd.read())

    template_file = os.path.join(config["template_dir"], template)

    # check if template exists
    if not os.path.isfile(template_file):
        click.secho("ERROR: Template file not found", bg="blue", fg="black")
        sys.exit(-1)

    # prepare data and pass it down
    payload = {
        "event": event,
        "event_date": event_date,
        "organizer": organizer,
        "venue": venue,
        "signatory": signatory,
        "template": template_file,
    }
    print_certificates(config, csv, payload)


if __name__ == "__main__":
    cli()
