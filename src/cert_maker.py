import jinja2
import pdfkit


def generate_pdf(html_file, output_file, options):
    options['log-level'] = 'none'
    return pdfkit.from_file(html_file, output_file, options=options)


def render_template(template_file, **kwargs):
    with open(template_file, "r") as fd:
        template = jinja2.Template(fd.read())
    return template.render(**kwargs)
