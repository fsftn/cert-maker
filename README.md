## e-Certificate Maker

Generate e-Certificates for participants of your events.


### Install Requirements

**Tip**: It is recommended to create a virtualenv, activate it & proceed to install and use.

```bash
# on debian/ubuntu based distros
sudo apt install wkhtmltopdf
pip3 install -r requirements.txt
```

### Usage

1. `cd src/`
2. `chmod +x make-cert.py`

```bash
./make-cert.py ../sample_data/data.csv \
--event "2 Day Python Workshop" \
--event-date "dd-mm-yyyy" \
--organizer "Free Software Foundation TamilNadu" \
--venue "XYZ College of Arts & Science, District" \
--signatory "Secretary, Org Name"
```

Generated PDFs can be found in the `output` folder. `build` folder is used as temporary workspace to create and destroy files to generate certificate. These two folders are safe to delete, if not required.

### Features

- [x]  Generate e-Certificate as PDF
- [ ]  Optionally e-mail the e-Certificate to the participant
